<?php declare(strict_types=1);

namespace App\Controllers;

use System\Core\Controller;

/**
 * Class Pages
 *
 * @property \App\Models\Pages $model
 *
 * @package App\Controllers
 */
class Pages extends Controller
{
    /**
     * @var \App\Models\Pages
     */
    private $model;

    public function __construct()
    {
        $this->model = new \App\Models\Pages();
    }

    public function getList(int $id = null)
    {
        $page = (null !== $id) ?: 0;

        /** @var \App\Models\Pages[] $data */
        $data = $this->model->findPerPage($page);

        if (is_null($data)) {
            $this->view('pages_list_empty');
        } else {
            $this->view('pages_list', ['data' => $data]);
        }
    }

    public function getItem(int $id)
    {
        var_dump($this->model->findOne($id));

        echo __METHOD__, $id;
    }

    public function addItem()
    {
        echo __METHOD__;
    }

    public function editItem(int $id)
    {
        echo __METHOD__, $id;
    }

    public function deleteItem(int $id)
    {
        echo __METHOD__, $id;
    }
}