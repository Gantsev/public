<?php declare(strict_types=1);

namespace App\Models;

use System\Core\Model;

/**
 * Class Pages
 *
 * @package App\Models
 */
class Pages extends Model
{
    private const ITEMS_PER_PAGE = 10;

    /**
     * Pages constructor
     *
     * @param array|null $data
     */
    public function __construct(array $data = null)
    {
        parent::__construct();

        $this->setStructure(new \System\Structures\Database\Pages);

        if (!is_null($data)) {
            $this->set($data);
        }
    }

    /**
     * @param int $id
     *
     * @return array|null
     * @access
     */
    public function findOne(int $id): ?array
    {
        $data = $this->db->findOne($this->structure->getPrimaryKey(), $id);

        return is_null($data) ? null : $this->set($data->toArray())->toArray();
    }

    /**
     * @param int $page
     *
     * @return array|null
     * @access
     */
    public function findPerPage(int $page): ?array
    {
        $data = [];

        $query = $this->db->query(sprintf(
            'SELECT %s FROM %s  ORDER BY %s ASC LIMIT %d OFFSET %d',
            implode(', ', $this->structure->getAllFields()),
            $this->structure->getTable(),
            $this->structure->getPrimaryKey(),
            self::ITEMS_PER_PAGE,
            $page * self::ITEMS_PER_PAGE
        ));

        while ($result = $query->row()) {
            $data[] = new self($result->toArray());
        }

        return empty($data) ? null : $data;
    }
}