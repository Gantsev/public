<?php declare(strict_types=1);

define('APP_PATH', __DIR__);

set_include_path(get_include_path() . PATH_SEPARATOR . APP_PATH);

require_once APP_PATH . '/System/Autoloader.php';
require_once APP_PATH . '/vendor/autoload.php';