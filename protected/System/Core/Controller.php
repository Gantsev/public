<?php declare(strict_types=1);

namespace System\Core;

abstract class Controller
{
    protected function view(string $template, array $data = null)
    {
        echo new View($template, $data);
    }
}