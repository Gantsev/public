<?php declare(strict_types = 1);

namespace System\Core;

/**
 * Class App
 *
 * @package System\Core
 */
class App
{
    /**
     * Точка входа
     *
     * @param Router $Router
     *
     * @throws \Exception
     *
     * @access public
     */
    public static function run(Router $Router)
    {
        $route = $Router->get();

        $controller = sprintf('App\Controllers\%s', $route['controller']);

        if (class_exists($controller)) {

            if (is_callable([$controller, $route['method']])) {

                if ($route['id']) {
                    call_user_func([(new $controller), $route['method']], (int)$route['id']);
                } else {
                    call_user_func([(new $controller), $route['method']]);
                }

            } else {
                throw new \Exception('Method is not callable: ' . $route['request']);
            }

        } else {
            throw new \Exception('Controller is not exists: ' . $controller);
        }
    }
}