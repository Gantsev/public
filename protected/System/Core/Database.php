<?php declare(strict_types=1);

namespace System\Core;

use System\Structures\Result;

class Database
{
    private static $connect;

    private $table;

    private $resultObject;
    /** @var \PDOStatement */
    private $resultResource;

    /**
     * Database constructor
     */
    public function __construct()
    {
        if (!(self::$connect instanceof \PDO))
        {
            self::$connect = new \PDO('mysql:dbname=test;host=127.0.0.1', 'root', 'root');
            self::$connect->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
    }

    /**
     * @param \System\Structures\Database $structure
     *
     * @return Database
     * @access
     */
    public function setStructure(\System\Structures\Database $structure): Database
    {
        $this->table = $structure->getTable();

        $inf = explode('\\', get_class($structure));
        $this->resultObject = sprintf('\System\Structures\Result\%s', end($inf));

        return $this;
    }

    /**
     * @param string $field
     * @param int $id
     *
     * @return Result|null
     * @access
     */
    public function findOne(string $field, int $id): ?Result
    {
        return $this->query(sprintf('SELECT * FROM %s WHERE %s = %d', $this->table, $field, $id))->row();
    }

    /**
     * @param string $query
     *
     * @return Database
     * @access
     */
    public function query(string $query): Database
    {
        $this->resultResource = self::$connect->query($query);

        return $this;
    }

    /**
     * @param string $query
     *
     * @return Database
     * @access
     */
    public function exec(string $query): Database
    {
        self::$connect->exec($query);

        return $this;
    }

    /**
     * @return Result|null
     * @access
     */
    public function row(): ?Result
    {
        $result = $this->resultResource->fetchObject($this->resultObject);

        if (!($result instanceof Result)) {
            return null;
        }

        return $result;
    }
}