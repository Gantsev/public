<?php declare(strict_types = 1);

namespace System\Core;

/**
 * Class Router
 *
 * @package System\Core
 */
class Router
{
    const DEFAULT_ROUTE = 'Pages/getList';

    /** @var array */
    private static $current = [
        'request' => null,
        'controller' => null,
        'method' => null,
        'id' => null,
    ];

    /** @var array */
    private $routes = [];

    /**
     * @return null|string
     * @access
     */
    public static function getMethod(): ?string
    {
        return self::$current['method'];
    }

    /**
     * @return null|string
     * @access
     */
    public static function getController(): ?string
    {
        return self::$current['controller'];
    }

    /**
     * @return null|string
     * @access
     */
    public static function getRequest(): ?string
    {
        return self::$current['request'];
    }

    /**
     * Добавляет маршрут
     *
     * @param string $url
     * @param string $route
     *
     * @return Router
     *
     * @throws \Exception
     *
     * @access public
     */
    public function add(string $url, string $route) : Router
    {
        if (preg_match('~^[a-z]+/[a-z_]+(/:num)?$~i', $route)) {
            $this->routes[$url] = $route;
        } else {
            throw new \Exception('Invalid route: ' . $route);
        }

        return $this;
    }

    /**
     * Возвращает текущий маршрут
     *
     * @return array
     *
     * @access public
     */
    public function get() : array
    {
        $this->prepare();
        return self::$current;
    }

    /**
     * Определяет текущий маршрут по url
     *
     * @access private
     */
    private function prepare(): void
    {
        $path = trim($_SERVER['REQUEST_URI'], '/');

        // Есть путь
        if ($path) {
            // Путь полностью совпадает с маршрутом
            if (isset($this->routes[$path])) {
                $this->setCurrent($path, $this->routes[$path], 0);
            } else {
                // Путь не найден, пробуем найти по маске
                $RouteIterator = new \ArrayIterator($this->routes);
                while ($RouteIterator->valid()) {
                    $route = str_replace([':id'], ['([0-9]+)'], $RouteIterator->key());
                    // Проверяем по регулярке
                    if (preg_match(sprintf('~^%s$~i', $route), $path, $params)) {
                        // Путь найден по маске
                        $this->setCurrent($path, $RouteIterator->current(), intval($params[1]));
                        break;
                    }
                    // След. роут
                    $RouteIterator->next();
                }
            }
        }
        // Роут по умолчанию
        if (is_null(self::$current['controller']) || is_null(self::$current['method'])) {
            $this->setCurrent($path, self::DEFAULT_ROUTE, 0);
        }
    }

    /**
     * Устанавливает текущий маршрут
     *
     * @param string $request
     * @param string $route
     * @param int $id
     *
     * @access private
     */
    private function setCurrent(string $request, string $route, int $id): void
    {
        preg_match('~^(?P<controller>[a-z]+)/(?P<method>[a-z]+)~i', $route, $split);

        self::$current = [
            'request' => $request,
            'controller' => $split['controller'],
            'method' => $split['method'],
            'id' => $id,
        ];
    }
}