<?php declare(strict_types=1);

namespace System\Core;
use System\Structures\Result;

/**
 * Class Model
 *
 * @property Database $db
 * @property \System\Structures\Database $structure
 *
 * @package System\Core
 */
abstract class Model
{
    /** @var Database */
    protected $db;

    /** @var \System\Structures\Database */
    protected $structure;

    /** @var array  */
    private $attributes = [];

    /**
     * Model constructor
     */
    protected function __construct()
    {
        $this->db = new Database();
    }

    /**
     * @param \System\Structures\Database $structure
     *
     * @return Model
     *
     * @access protected
     */
    protected function setStructure(\System\Structures\Database $structure): Model
    {
        $this->structure = $structure;
        $this->db->setStructure($structure);
        return $this;
    }

    /**
     * @param iterable $attributes
     *
     * @return Model
     * @throws \Exception
     * @access
     */
    public function set(iterable $attributes): Model
    {
        if (!($this->structure instanceof \System\Structures\Database)) {
            throw new \Exception('The structure is not loaded');
        }

        foreach ($attributes as $name => $value)
        {
            if (!$this->structure->isExists($name)) {
                throw new \Exception(sprintf('Field [%s] is not exists in structure', $name));
            }

            if (\System\Structures\Database::PARAM_INT === $this->structure->getType($name)) {
                $this->attributes[$name] = is_null($value) ? null : intval($value);
            }

            if (\System\Structures\Database::PARAM_STR === $this->structure->getType($name)) {
                $this->attributes[$name] = is_null($value) ? null : strval($value);
            }
        }

        return $this;
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws \Exception
     * @access
     */
    public function get(string $name)
    {
        if (!isset($this->attributes[$name])) {
            throw new \Exception(sprintf('Field [%s] is not exists in attributes', $name));
        }

        return $this->attributes[$name];
    }

    /**
     * @param string $name
     *
     * @return string
     * @access
     */
    private function trimMaxLength(string $name): string
    {
        if (mb_strlen($this->attributes[$name]) > $this->structure->getLength($name)) {
            return mb_substr($this->attributes[$name], $this->structure->getLength($name));
        } else {
            return $this->attributes[$name];
        }
    }

    /**
     * @return array
     * @throws \Exception
     * @access
     */
    protected function prepareDataToInsert(): array
    {
        $data = [];

        foreach ($this->attributes as $name => $value)
        {
            if (!$this->structure->isExists($name)) {
                throw new \Exception(sprintf('Field [%s] is not exists in structure', $name));
            }

            if ($this->structure->checkFlag($name, \System\Structures\Database::FLAG_NO_FLAGS))
            {
                $data[$name] = $value;
                continue;
            }

            if ($this->structure->checkFlag($name, \System\Structures\Database::FLAG_NOT_NULL) && is_null($value))
            {
                throw new \Exception(sprintf('Field [%s] can not be NULL value', $name));
            }

            if ($this->structure->checkFlag($name, \System\Structures\Database::FLAG_CAST_NULL_TO_STRING) && is_null($value))
            {
                $data[$name] = '';
            }

            if ($this->structure->checkFlag($name, \System\Structures\Database::FLAG_IS_AUTOINCREMENT))
            {
                $data[$name] = null;
            }

            if ($this->structure->checkFlag($name, \System\Structures\Database::FLAG_DATETIME))
            {
                $Date = (!$value) ? new \DateTime() : new \DateTime($this->attributes[$name]);
                $data[$name] = $Date->format('Y-m-d H:i:s');
            }

            if (null !== $this->structure->getLength($name))
            {
                $data[$name] = $this->trimMaxLength($name);
            }
        }

        return $data;
    }

    public function toArray(): array
    {
        return $this->attributes;
    }

    protected function insert(): Model
    {

    }
}