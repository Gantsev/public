<?php declare(strict_types=1);

namespace System\Core;

class View
{
    /** @var string */
    private $template;
    /** @var array */
    private $data;
    /** @var \Twig_Environment */
    private $twig;

    /**
     * View constructor
     *
     * @param string $template
     * @param array|null $data
     */
    public function __construct(string $template, array $data = null)
    {
        $this->template = $template;
        $this->data     = $data;

        $this->twig = new \Twig_Environment(
            new \Twig_Loader_Filesystem(sprintf('%s/App/Views', APP_PATH)),
            [
                'debug' => true,
            ]
        );

        $this->twig->addExtension(new \Twig_Extension_Debug);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->twig->render($this->template . '.twig', $this->data);
    }
}