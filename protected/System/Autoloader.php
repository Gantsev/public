<?php declare(strict_types=1);

namespace System;

/**
 * Class Autoloader
 *
 * @package System\Core
 */
class Autoloader
{
    /**
     * @param $className
     */
    static public function loader(string $className)
    {
        $filename = sprintf('%s/%s.php', APP_PATH, str_replace('\\', '/', $className));
        if (file_exists($filename)) {
            require_once $filename;
        } else {
            exit($filename . ' is not found!');
        }
    }
}

\spl_autoload_register('System\Autoloader::loader');