<?php declare(strict_types=1);

namespace System\Structures;

/**
 * Class Database
 *
 * @package System\Structures
 */
abstract class Database
{
    public const PARAM_INT = \PDO::PARAM_INT;

    public const PARAM_STR = \PDO::PARAM_STR;

    public const FLAG_NO_FLAGS = 1;

    public const FLAG_PROTECTED = 2;

    public const FLAG_NOT_NULL = 4;

    public const FLAG_DATETIME = 8;

    public const FLAG_DATETIME_NEED_UPDATE = 16;

    public const FLAG_IS_AUTOINCREMENT = 32;

    public const FLAG_CAST_NULL_TO_STRING = 64;

    protected $pk = 'id';
    /** @var string */
    protected $table = '';
    /** @var array */
    protected $fields = [];

    /**
     * @return string
     * @access protected
     */
    final public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @param string $field
     *
     * @return bool
     *
     * @access public
     */
    final public function isExists(string $field): bool
    {
        return isset($this->fields[$field]);
    }

    /**
     * @param string $field
     *
     * @return int
     *
     * @throws \Exception
     *
     * @access public
     */
    final public function getType(string $field): int
    {
        if (isset($this->fields[$field]['type'])) {
            return $this->fields[$field]['type'];
        } else {
            throw new \Exception(sprintf('For the field [%s] is not set the data type', $field));
        }
    }

    /**
     * @param string $field
     *
     * @return int
     *
     * @throws \Exception
     *
     * @access public
     */
    final public function getFlags(string $field): int
    {
        if (isset($this->fields[$field]['flags'])) {
            return $this->fields[$field]['flags'];
        } else {
            throw new \Exception(sprintf('For the field [%s] is not set the flags', $field));
        }
    }

    /**
     * @param string $field
     * @param int $flag
     *
     * @return bool
     *
     * @throws \Exception
     *
     * @access public
     */
    final public function checkFlag(string $field, int $flag): bool
    {
        return boolval($this->getFlags($field) & $flag);
    }

    /**
     * @param string $field
     *
     * @return int
     *
     * @throws \Exception
     *
     * @access public
     */
    final public function getLength(string $field): ?int
    {
        return $this->fields[$field]['length'] ?? null;
    }

    public function getPrimaryKey(): string
    {
        return $this->pk;
    }

    public function getAllFields(): array
    {
        return array_keys($this->fields);
    }
}