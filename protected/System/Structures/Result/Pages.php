<?php declare(strict_types=1);

namespace System\Structures\Result;

use System\Structures\Result;

/**
 * Class Page
 *
 * @package System\Structures\DataObject
 */
class Pages extends Result
{
    /** @var int */
    protected $pageId;
    /** @var string */
    protected $title;
    /** @var string */
    protected $body;
    /** @var string */
    protected $keywords;
    /** @var string */
    protected $modified;
}