<?php declare(strict_types=1);

namespace System\Structures\Database;

use System\Structures\Database;

/**
 * Class Pages
 *
 * @property array $fields
 *
 * @package System\Structures\Database
 */
class Pages extends Database
{
    /** @var string */
    protected $table    = 'pet__page';

    protected $pk       = 'pageId';

    /** @var array */
    protected $fields   = [
        'pageId' => [
            'type'      => Database::PARAM_INT,
            'flags'     => Database::FLAG_PROTECTED | Database::FLAG_IS_AUTOINCREMENT,
        ],
        'title' => [
            'type'      => Database::PARAM_STR,
            'length'    => 255,
            'flags'     => Database::FLAG_NOT_NULL,
        ],
        'body' => [
            'type'      => Database::PARAM_STR,
            'length'    => 16777215,
            'flags'     => Database::FLAG_NOT_NULL,
        ],
        'keywords' => [
            'type'      => Database::PARAM_STR,
            'length'    => 255,
            'flags'     => Database::FLAG_NOT_NULL | Database::FLAG_CAST_NULL_TO_STRING,
        ],
        'modified' => [
            'type'      => Database::PARAM_STR,
            'flags'     => Database::FLAG_DATETIME | Database::FLAG_DATETIME_NEED_UPDATE,
        ]
    ];
}