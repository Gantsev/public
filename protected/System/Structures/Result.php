<?php declare(strict_types=1);

namespace System\Structures;

/**
 * Class DataObject
 *
 * @package System\Structures
 */
abstract class Result
{
    /**
     * @return array
     * @access
     */
    final public function toArray() : array
    {
        return get_object_vars($this);
    }

    /**
     * @param string $var
     *
     * @return mixed|bool
     * @access
     */
    final public function get(string $var)
    {
        return $this->{$var} ?? false;
    }
}