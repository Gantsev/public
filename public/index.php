<?php declare(strict_types=1);

require_once __DIR__ . '/../protected/bootstrap.php';

\System\Core\App::run(
    (new \System\Core\Router)
        ->add('list', 'Pages/getList')
        ->add('list/:id', 'Pages/getList')
        ->add('get/:id', 'Pages/getItem')
        ->add('add', 'Pages/addItem')
        ->add('edit/:id', 'Pages/editItem')
        ->add('delete/:id', 'Pages/deleteItem')
);
